# we're going to perform the QDA

import pandas as pd
import numpy as np
import csv
# importing the  data
tr_data = pd.read_csv('train_all.csv')
test_data = pd.read_csv('test_all.csv')
x_train  = tr_data.iloc[:, 1:4]
y_train  = tr_data.iloc[:, -1]
x_test = test_data.iloc[:, 1:4]
#converting them into array 
X = x_train.values
Y = y_train.values
Xt = x_test.values
# getting the mean and the covariance
# we need to get the mean matrix and the covariance matrix for reach class
result = []
mean_mat = []
cov_mat = []
classes =  np.unique(Y)
prob = []
for i in classes:
    xNew = X[Y.flatten() == i, :]
    prob.append(xNew.size/ X.size)
    mean_mat.append(xNew.mean(0))   
    cov_mat.append(np.cov((xNew.T)))
means = np.asarray(mean_mat)
prob  = np.asarray(prob)

threshold = np.add((2*(prob[1]/prob[0])),np.divide(np.linalg.det(cov_mat[0]) ,
                    np.linalg.det(cov_mat[1])))
for x in Xt:
    quadraticTerm = np.matmul(np.matmul(x.T,np.subtract(np.linalg.inv(cov_mat[1])
                            , np.linalg.inv(cov_mat[0]))), x)
    linearTerm = np.matmul(np.multiply(2, x.T),np.subtract(np.matmul(np.linalg.inv(cov_mat[1]), means[1]),
                                   np.matmul(np.linalg.inv(cov_mat[0]), means[0])))
    totalLinearTerm = np.ma.add(linearTerm , np.ma.subtract(
            np.matmul(np.matmul(means[1].T,np.linalg.inv(cov_mat[1])), means[1]),
            np.matmul(np.matmul(means[0].T,np.linalg.inv(cov_mat[0])),means[0])))
    qda = np.subtract(quadraticTerm, totalLinearTerm)
    if(qda > threshold):
        result.append(1)
    else:
        result.append(2)
        

with open('classificationresult.csv','w') as f:   
        wr = csv.writer(f)
        wr.writerows(zip(result))
