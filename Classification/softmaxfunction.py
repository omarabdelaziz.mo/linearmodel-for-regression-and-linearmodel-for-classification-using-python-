#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 23:29:17 2018

@author: omarabdelaziz
"""

import pandas as pd 
import numpy as np
import scipy.sparse

def softMax(z):
    z -= np.max(z)
    sm = (np.exp(z)/ np.sum(np.exp(z))).T
    return sm

z = [1 , -2,0.5]
z = np.asarray(z)

print(softMax(z))