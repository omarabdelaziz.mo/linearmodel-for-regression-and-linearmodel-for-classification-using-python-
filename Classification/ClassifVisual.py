#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 00:59:23 2018

@author: rootkit
"""


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
data = pd.read_csv('train_all.csv')
data_class = data.values

y_train  = data.iloc[:, -1]

x_train  = data.iloc[:, 1:4]
X = x_train.values
Y = y_train.values
classes =  np.unique(Y)
'''
data.hist(figsize=(10, 10))
plt.savefig('Histogram_Distribution.pdf')
plt.show()
'''

'''
data.plot(kind='density', subplots=True, layout=(4,4), sharex=(False), title='Density_Distribution', figsize=(10, 10))

plt.savefig('Density_Dis.pdf')
plt.show()
'''


# correlation
for i in classes:
    xNew = data_class[Y.flatten() == i, :]
    corr = xNew[:, 1:]
    correlation  = np.corrcoef(xNew.T)
    fig =  plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    cax = ax.matshow(correlation, vmin=-1, vmax=1)
    fig.colorbar(cax)
    ticks = np.arange(0,4,1)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_xticklabels(['B	','G'	,'R','class'])
    ax.set_yticklabels(['B	','G'	,'R','class'])
    plt.show()
    fig.savefig('Correlation_matrix.pdf')

'''
pd.plotting.scatter_matrix(data, figsize= [15,15], s=.2, diagonal='kde')
plt.savefig('ScatterMatrix.pdf')
plt.show()
'''