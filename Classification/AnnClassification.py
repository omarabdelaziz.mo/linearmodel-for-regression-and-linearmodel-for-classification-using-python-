#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  2 16:43:42 2018

@author: rootkit
"""
from keras.models import Sequential, Model
from keras.layers import Dense, Input
from keras.utils import to_categorical

import pandas as pd

train_data = pd.read_csv('train_all.csv')
test_data = pd.read_csv('test_all.csv')

X_train =  train_data.iloc[:, 1:4]
Y_train = train_data.iloc[:, -1]
Y_train = Y_train.values
Y_train = to_categorical(Y_train)
X_test = test_data.iloc[:, 1:]

#model difinition

inp = Input(shape=(3,))
hidden_1 = (Dense(4, activation='relu')(inp))
hidden_2 = (Dense(8, activation='relu')(hidden_1))
out = (Dense(3, activation='softmax')(hidden_2))
model = Model(input=inp, output=out)
# compile
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# fitting 
model.fit(X_train, Y_train)

y_predict = model.predict(X_test)
print(y_predict)