#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  2 18:18:28 2018

@author: rootkit
"""

from sklearn.ensemble import RandomForestRegressor
import pandas as pd
import csv
train_data = pd.read_csv('reg_train.csv')
test_data = pd.read_csv('reg_test.csv')
X_train = train_data.iloc[:, 2:14]
Y_train = train_data.iloc[:, -1]
X_test = test_data.iloc[:, 2:]
# Convert to array
X_train = X_train.values
Y_train = Y_train.values
X_test = X_test.values

regressor= RandomForestRegressor(n_estimators=1500, min_samples_split=2)
regressor.fit(X_train, Y_train)
res = regressor.predict(X_test)
res = res.astype(int)
with open('randomforest.csv','w') as f:   
        wr = csv.writer(f)
        wr.writerows(zip(res))
        


