#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  2 23:36:10 2018

@author: rootkit
"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
data = pd.read_csv('reg_train.csv')


'''
data.hist(figsize=(10, 10))
plt.savefig('Histogram_Distribution.pdf')
plt.show()
'''

'''
data.plot(kind='density', subplots=True, layout=(4,4), sharex=(False), title='Density_Distribution', figsize=(10, 10))

plt.savefig('Density_Dis.pdf')
plt.show()
'''

'''
# correlation
corr = data.iloc[:, 1:]
correlation  = corr.corr()
fig =  plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
cax = ax.matshow(correlation, vmin=-1, vmax=1)
fig.colorbar(cax)
ticks = np.arange(0,16,1)
ax.set_xticks(ticks)
ax.set_yticks(ticks)
ax.set_xticklabels(['dteday'	,'season','yr','mnth','hr','holiday','weekday','workingday	','weathersit','temp','atemp','hum','windspeed	','casual','registered','cnt'
])
ax.set_yticklabels(['dteday'	,'season','yr','mnth','hr','holiday','weekday','workingday	','weathersit','temp','atemp','hum','windspeed	','casual','registered','cnt'
])
plt.show()
fig.savefig('Correlation_matrix.pdf')
'''
'''
pd.plotting.scatter_matrix(data, figsize= [15,15], s=.2, diagonal='kde')
plt.savefig('ScatterMatrix.pdf')
plt.show()
'''
