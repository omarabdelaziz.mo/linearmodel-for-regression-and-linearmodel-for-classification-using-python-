#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 30 04:26:02 2018

@author: rootkit
"""


import pandas as pd
import numpy as np
import csv
#equation => y = b0 +  b1 * x

tr_data = pd.read_csv('reg_train.csv')
test_data = pd.read_csv('reg_test.csv')
instant = tr_data.iloc[:, 0]
#train_data
tr_data =tr_data.drop(['instant', 'dteday','registered','casual'], axis=1)
X = tr_data.iloc[:, 0:-1]
Y = tr_data.iloc[:, -1]
x_train  = X.values
y_train = Y.values
#test_data
test_data =test_data.drop(['instant', 'dteday'], axis=1)
X = test_data.iloc[:, :]

x_test  = X.values

def linearRegression(x_train, y_train, x_test):
    one = np.ones((len(x_train),1))
    xNew = np.hstack((one, x_train))
    inv = np.linalg.inv(np.matmul(xNew.T , xNew))   
    sec = np.matmul(xNew.T, y_train)
    b = np.matmul(inv, sec)
    tsOnes = np.ones((len(x_test),1))
    xt = np.hstack((tsOnes, x_test))
    res =np.matmul(b , xt.T)
    return res

res = linearRegression(x_train , y_train, x_test)
res = res.astype(int)
with open('result1.csv','w') as f:   
        wr = csv.writer(f)
        wr.writerows(zip(res))
        
